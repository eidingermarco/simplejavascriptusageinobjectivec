#[SimpleJavaScriptUsage](https://bitbucket.org/eidingermarco/mynoteswebapp)
A project to illustrate the usage of javascript code in an objective-c application.
  
### Main Use Case

- Run javascript code to evaluate if a certain field shall be visible or not

### Used Framework

The following framework is used (available since iOS 7.0)

```
#import <JavaScriptCore/JavaScriptCore.h>
```

### Application logic

- File `isFieldVisibleForAccount.js` is included as resource
- Method `-(BOOL)isFieldVisibleInC4COfflineMode:(NSString*)fieldname` executes the javascript
- Method `+(NSString*)valueForKeyInAccount:(NSString*)key` is called by javascript


## Additional info
- [JavaScriptCore tutorial](example based on http://www.bignerdranch.com/blog/javascriptcore-example/): good tutorial which inspired this project
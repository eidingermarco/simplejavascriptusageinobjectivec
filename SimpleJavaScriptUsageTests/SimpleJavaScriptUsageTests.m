//
//  SimpleJavaScriptUsageTests.m
//  SimpleJavaScriptUsageTests
//
//  Created by Eidinger, Marco on 7/18/14.
//  Copyright (c) 2014 SAP. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface SimpleJavaScriptUsageTests : XCTestCase

@end

@implementation SimpleJavaScriptUsageTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end

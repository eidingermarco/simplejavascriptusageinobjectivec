//
//  ViewController.m
//  SimpleJavaScriptUsage
//
//  Created by Eidinger, Marco on 7/18/14.
//  Copyright (c) 2014 SAP. All rights reserved.
//

#import "ViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    NSString *fieldname = @"formattedName";
    
    BOOL isFieldVisible = [self isFieldVisibleInC4COfflineMode:fieldname];
    
    NSLog(@"Field  %@ is visible: %s", fieldname, isFieldVisible ? "true" : "false");
    
    fieldname = @"Does not exist";
    
    isFieldVisible = [self isFieldVisibleInC4COfflineMode:fieldname];
    
    NSLog(@"Field  %@ is visible: %s", fieldname, isFieldVisible ? "true" : "false");
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark example for C4C usage - run javascript to determine field visibility
#pragma mark -

-(BOOL)isFieldVisibleInC4COfflineMode:(NSString*)fieldname {
    
    JSContext *context = [[JSContext alloc] init];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"isFieldVisibleForAccount" ofType:@"js"];
    NSString *jsCode = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    [context setExceptionHandler:^(JSContext *context, JSValue *value) {
        NSLog(@"JS: %@", value);
    }];
    
    context[@"valueForKey"] = ^(NSString *thingType,NSString *key)
    {
        if ([thingType isEqualToString:@"COD_ACCOUNT_TT"]) {
            return [ViewController valueForKeyInAccount:key];
        }
        return @"";
    };
    
    [context evaluateScript:jsCode];
    
    JSValue * func =context[@"isFieldVisible"];
    NSArray * args = @[fieldname];
    JSValue * ret =[func callWithArguments:args];

    BOOL isVisible = [ret toBool];
    
    return isVisible;

}

#pragma mark -
#pragma mark example for C4C usage - callback for javascript to request value(s) stored on device
#pragma mark -

+(NSString*)valueForKeyInAccount:(NSString*)key {
    
    if ([key isEqualToString:@"formattedName"]) {
        return @"Hamburger SV";
    }
    return @"";
}

#pragma mark -
#pragma mark additional simple examples
#pragma mark -

-(void)testSimpleExamples {
    
    BOOL isValid = [ViewController isValidNumber:@"650"];

    isValid = [ViewController isValidNumber:@"650 422-4410"];
    
    [self exampleToCallObjectiveCBlocksInJavaScript];
}

// example based on http://www.bignerdranch.com/blog/javascriptcore-example/

+ (BOOL)isValidNumber:(NSString *)phone
{
    // getting a JSContext
    JSContext *context = [JSContext new];
    
    // enable error logging
    [context setExceptionHandler:^(JSContext *context, JSValue *value) {
        NSLog(@"JS: %@", value);
    }];
    
    // defining a JavaScript function
    NSString *jsFunctionText =
    @"var isValidNumber = function(phone) {"
    "    var phonePattern = /^[0-9]{3}[ ][0-9]{3}[-][0-9]{4}$/;"
    "    return phone.match(phonePattern) ? true : false;"
    "}";
    [context evaluateScript:jsFunctionText];
    
    // calling a JavaScript function
    JSValue *jsFunction = context[@"isValidNumber"];
    JSValue *value = [jsFunction callWithArguments:@[ phone ]];
    
    return [value toBool];
}

-(void)exampleToCallObjectiveCBlocksInJavaScript {
    
    JSContext *context = [[JSContext alloc] init];
    
    //we are telling that "sum" is function, takes two arguments
    context[@"sum"] = ^(int arg1,int arg2)
    {
        return arg1+arg2;
    };
    
    NSString * jsCode =@"sum(4,5);";
    JSValue * sumVal = [context evaluateScript:jsCode];
    
    NSLog(@"Sum(4,5) = %d", [sumVal toInt32]);
    
    //we are telling that "getRandom" is function, does not take any arguments
    context[@"getRandom"] = ^()
    {
        return rand()%100;
    };
    
    NSString * jsCode2 =@"getRandom();";
    for(int i=0;i<5;i++)
    {
        JSValue * sumVal2 = [context evaluateScript:jsCode2];
        NSLog(@"Random Number = %d", [sumVal2 toInt32]);
    }
}

@end
